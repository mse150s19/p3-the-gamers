--Summary of Code--

The point of this project is to have our code do each of the following:
    "Clean" the data edges, to make the data easier to evaluate.
    Locate Mossbauer fluroescence peaks, using peak finding methods.
    Fit peaks with lorentzians equation.
    Fit Gaussian profiles to the peaks.

The offset values and peaks are in a list because each of the three plots are different for these parameters, so in the for loop, we made it so that the loop would run the plots with three different values for each of them.

The parameters of the graphs are shown as [850:5070]
Smooth and ysmooth are two different functions, but they both basically do the same thing to smooth the two lines in the plots of each graph, just to make it look more aesthetically pleasing.
The gaussian fit is also in the code, using the equation that was given to us in the project summary, refer to the project 3 objectives for more information.

--Assumptions for Code to Work for New Users--

The assumptions that are made for this code is that the data is going to be given in the order of IronFoilCalc, Fe2O3Calc, then Fe3O4Calc, and these must be in the correct order or else the code will be confused. You also must have lmfit installed on your computer for the plots to plot.
Another assumption that is needed is for users to download the lmfit, this can be done by using the command $pip install --user lmfit

--Output of the Code for Comparison to New Users--

The output of this code will show 3 plots one after the other, with channel on the x-axis and values on the y-axis. You will be able to see the lorentzian fit in red, and the minimum values are also plotted. These will save as a .png and as an excel sheet after being closed. Once one plot is closed, the next plot will show and on and on.
To plot the code, run $python plot_ha.py in order to show all of the plots, and when this command is written, the parameters for the peak finding show up on the terminal, along with the lorentzian fit, and the gaussian fit. In our code for plot_ha.py, there is a commented out section at the bottom, that should be neglected, it was just some experimentation from our group.

