
import pylab as plb
import matplotlib.pyplot as plt
import pandas as pd
import scipy
import glob
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter
from scipy import asarray as ar,exp
from scipy.signal import find_peaks
import numpy as np
import lmfit
from numpy import exp, loadtxt, pi, sqrt
from lmfit import Model
from lmfit.models import LinearModel, LorentzianModel

sheets = ['IronFoilCalc','Fe2O3Calc','Fe3O4Calc']
file_name = "Final_analysis.xlsx"

offsets = [6135,6150,6200]

for num, i in enumerate (sheets):
	df = pd.read_excel(file_name, sheetname=i)
	x = df['channel'][850:5070]
	y = df['value'][850:5070]
	z = df['quad'][850:5070]
	x,y,z = np.array(x), np.array(y),np.array(z)

	detrend = y-z
	#mean = sum(x*detrend)/sum(detrend)
	#sigma = np.sqrt(sum(x*(detrend-mean)**2)/sum(detrend))
	#print(mean,sigma)

	smooth = savgol_filter(detrend,101,6)

	offset = offsets[num]
	ysmooth = scipy.signal.savgol_filter(detrend,201,3)

	peaks =[scipy.signal.find_peaks(-ysmooth, height=100, distance=75),scipy.signal.find_peaks(-ysmooth, height=10, distance=100, width=24),scipy.signal.find_peaks(-ysmooth, height=10, distance=100, width=24)]
	
	peak_pos = peaks[num][0]
	peak_pos_shifted = [peak + offset for peak in peak_pos]
	peak_vals = peaks[num][1]['peak_heights']
	peak_vals = -peak_vals

	print(peak_pos_shifted)
	def gaussian(x, amp, cen, wid):
		"""1-d gaussian: gaussian(x, amp, cen, wid)"""
		return (amp / (sqrt(2*pi) * wid)) * exp(-(x-cen)**2 / (2*wid**2))
	gmodel = Model(gaussian)
	result = gmodel.fit(y, x=x, amp=5, cen=5, wid=1)
	
	print(result.fit_report())
	
	loren_model = LorentzianModel()
	pars = loren_model.guess(ysmooth, x=x)
	
	loren1 = LorentzianModel(prefix= 'peak1_')
	pars.update(loren1.make_params())
	pars['peak1_center'].set(7324, min=7200, max=7500)

	def gaussian(x, amp, cen, wid):
		"""1-d gaussian: gaussian(x, amp, cen, wid)"""
		return (amp / (sqrt(2*pi) * wid)) * exp(-(x-cen)**2 / (2*wid**2))
	gmodel = Model(gaussian)
	result = gmodel.fit(y, x=x, amp=5, cen=5, wid=1)

	print(result.fit_report())

	plt.plot(x,detrend,'g+:')
	plt.plot(peak_pos_shifted, peak_vals, 'bo')
	plt.plot(x,ysmooth,'r-',label='fit')
	plt.plot(x, result.init_fit, 'k--')
	plt.plot(x, result.best_fit, 'r-')
	plt.legend()
	plt.title('Mossbauer Effect')
	plt.xlabel('channel')
	plt.ylabel('value')
	plt.show()

#sheet1 = ['Fe3O4Calc']

#x1 = df['channel'][750:5070]
#y1 = df['value'][750:5070]
#z1 = df['quad'][750:5070]

#detrend1 = y1-z1

#smooth1 = savgol_filter(detrend1,101,6)

#def gfit(x1, amp, cen, wid):
	#"""1-d gfit: gfit(x1, amp, cen, wid)"""
	#return (amp / (sqrt(2*pi) * wid)) * exp(-(x1-cen)**2 / (2*wid**2))
#gfitmodel = Model(gfit)
#result1 = gfitmodel.fit(y1, x1=x1, amp=5, cen=5, wid=1)

#print(result1.fit_report())


#plt.plot(x1,detrend1,'g+:')

#plt.plot(x1,smooth1,'r-', label='fit')
#plt.plot(x1, result1.init_fit, 'k--')
#plt.plot(x1, result1.best_fit, 'r-')
#plt.legend()
#plt.title('Mossbauer Effect')
#plt.xlabel('channel')
#plt.ylabel('value')
#plt.show()

